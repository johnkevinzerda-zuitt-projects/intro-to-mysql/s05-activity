SELECT customerName
FROM customers
WHERE country = "Philippines";

SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP
FROM products
WHERE productName = "The Titanic";

SELECT firstName, lastName
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName
FROM customers
WHERE state IS NULL;

SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA" AND creditLimit > 3000;

SELECT customerName
FROM customers
WHERE customerName NOT LIKE "%A%";

SELECT customerNumber
FROM orders
WHERE comments LIKE "%DHL%";

SELECT productLine
FROM productlines
WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country
FROM customers;

SELECT DISTINCT status
FROM orders;

SELECT customerName, country
FROM customers
WHERE country IN("USA", "France", "Canada");

SELECT employees.firstName AS firstName, employees.lastName AS lastName, offices.city AS officeCity
FROM employees JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.city = "Tokyo";

SELECT customers.customerName
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.lastName = "Thompson" AND employees.firstName = "Leslie";

SELECT products.productName, customers.customerName
FROM customers JOIN orders
ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orderdetails.orderNumber = orders.orderNumber
JOIN products ON products.productCode = orderdetails.productCode
WHERE customers.customerName = "Baane Mini Imports";

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN offices ON offices.officeCode = employees.officeCode
WHERE customers.country = offices.country;

SELECT lastName, firstName
FROM employees
WHERE reportsTo = 1143;

SELECT productName, MSRP
FROM products
ORDER BY MSRP DESC
LIMIT 1;

SELECT COUNT(*) AS numberOfCustomersInUK
FROM customers
WHERE country = "UK";

SELECT productLine, COUNT(productName) AS numberOfProduct
FROM products
GROUP BY productLine;

SELECT employees.firstName, employees.lastName, COUNT(customers.customerNumber) AS numberOfServedCustomer
FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
GROUP BY employees.employeeNumber;

SELECT productName, quantityInStock
FROM products 
WHERE productLine = "Planes" AND quantityInStock < 1000;